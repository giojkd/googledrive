<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Facades\Http;
use Spatie\SimpleExcel\SimpleExcelReader;

class GoogleDrive extends Controller
{
    var $client;
    public function __construct()
    {
        $this->client = new \Google_Client;
        $this->client->setClientId(env('GOOGLE_CLIENT_ID'));
        $this->client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        #    $this->client->refreshToken(env('GOOGLE_DRIVE_REFRESH_TOKEN'));
    }

    public function handle()
    {

        $files = Storage::drive('google')->files("/");

        foreach ($files as $key => $fileId) {
            $pathToFile = Storage::drive('google')->url($fileId);
            $COLUMN_INDEX = 'A';
            $COLUMN_QUANTITY = 'C';
            $response = Http::get($pathToFile);
            $time = time();
            $file = $time . '.xlsx';
            Storage::disk('local')->put($file, $response->getBody());

            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(Storage::disk('local')->path($file));
            $worksheet = $spreadsheet->getActiveSheet();

            $quantityUpdates = [];
            foreach ($worksheet->getRowIterator() as $row) {

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE);


                foreach ($cellIterator as $cellLetter => $cell) {


                    if ($cellLetter == $COLUMN_INDEX) {
                        $index = $cell->getValue();
                    }
                    if ($cellLetter == $COLUMN_QUANTITY) {

                        $value = $cell->getValue();
                    }
                }
                if (!is_null($index))
                    $quantityUpdates[] = ['index' => $index, 'value' => $value];
            }

            $token = 'adiufhakeijvbkweijrvnlwkejrvnlwkejrnlvwejrnvlwkrnlvkwjfnrlkjsnelckaneròiocjvnwretlbjuvoi4uh5bvo2783h45ro8nquw3cjq34lioufh2o08459ghvo823iw45o3pjdfclghkrvno2hiuvow458piròejkvcnow8i4l5uhfgjplvw9òior3hcfnow8il4u5reohgjvnowil45uiruehjvpwo34ioòeròcjkmowl4iu5trghjvnoweirjfpw9';
            $quantityUpdates = collect($quantityUpdates);
            if ($quantityUpdates->count() > 0) {
                $counter = 0;
                $quantityUpdates->each(function ($item) use ($counter) {

                    $client = new \GuzzleHttp\Client();
                    $url = "http://erp.amtitalia.com/admin/api/set-availability.php";

                    $myBody['manufacturer_id'] = '130,132,124,126';
                    $myBody['supplier_id'] = 53;
                    $myBody['reference_code'] = $item['index'];
                    $myBody['quantity'] = $item['value'];
                    $request = $client->post($url,  ['form_params' => $myBody]);
                    $getBody = $request->getBody();


                });
            }
        }
    }
}
/*
array:6 [▼
  "access_token" => "ya29.a0AfH6SMBwPTMcMAK7W0ci5K1BgsJZx6isiCaLUlYBNNq49rsW96OeiDG2fHSVwSi3sDEaaN7OYlVDmhFG5XQPibBO4uy2ZsDMwEDPvkTnepKna8C3VjaGHr7JyVrD_IckA1CWEAdrJ840etTpl1AK28Qaf ▶"
  "expires_in" => 3599
  "refresh_token" => "1//03_GKE9j9vAO_CgYIARAAGAMSNwF-L9Ir5-lDZIaOKJfDLOQ2sXxT1A_ARte5VjnM9kBzSFKP-9KZOVF4vOWfRN2-LqdQQn_eewM"
  "scope" => "https://www.googleapis.com/auth/drive.scripts https://www.googleapis.com/auth/drive.metadata https://www.googleapis.com/auth/drive.appdata https://www.googleapi ▶"
  "token_type" => "Bearer"
  "created" => 1589287047
]
*/
